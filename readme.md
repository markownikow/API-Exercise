### Requirements
 - docker engine and docker-compose
 - kubectl 
 - sufficient cpus and memory available ex with local cluster.(`` minikube start --cpus 3 --memory 2048 ``)
 - single node cluster and current config context pointing to it - There is hostPath PersistentVolume which is supported for single node cluster


### Build and Run(docker)

in root project directory run following command:

```bash

docker-compose up --build .

```

This will create and run two containers: database (titanic-api-db) and rest API application 
accessible at **http://localhost:8090/people**

The Application image is built based on Dockerfile defined in root project dir. First intermediate container
is used to build jar package with Gradle. Then actual image is made based on jre image and 
 output jar file received in first build step

#### How data are loaded
The table structure can be found in schema.sql file. This file together with importdata.sh are mapped
as a volumes into /docker-entrypoint-initdb.d container directory - the mysql container entrypoint
executes files automatically. 



### Kubernetes

For kubernetes part project has separate "kubernetes" directory containing all
necessary resources definitions required to deploy app to cluster.

From  *kubernetes* directory and run

```bash

kubectl apply -f .
```

It will start mysql and replicated API application pods. API Application container are using pre-built docker image
 (built form Dockerfile in root dir )
pushed to public repo in Dockerhub. 
For  initializing database data - there are config maps created and mapped as volumes. Since config maps volumes
are readonly there is initContainer directive which copies files into /docker-entrypoint-initdb.d

InnitContainer together with readinessProbe feature are also configured in order to ensure
that API application doesn't start before database pod is accessible

In order to access app (minikube):

``` 
minikube service titanic-api-app --url
```
then navigate to */people* url

### Application

Stack:
 * Spring Boot
 * Mysql 8
 * JDK 11
 * Gradle
 * Docker
 * Minikube (cluster version 1.12.4)
 * Kotlin



Endpoints has been implement in PeopleController.kt utilizing Spring framework annotations. The data model is mapped in Person.kt class
with JPA annotations. The @Repository annotation (PeopleRepository.kt) ensures load and save operations for given *Person* object automatically 
populated from request body (@RequestBody annotation)