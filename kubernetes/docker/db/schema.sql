create table if not exists people
(
  uuid                       varchar(32)  DEFAULT (uuid()) not null primary key,
  age                        int          not null,
  fare                       double       not null,
  name                       varchar(64)  not null,
  parents_or_children_aboard int          not null,
  passenger_class            int          not null,
  sex                        char(6)      not null,
  siblings_or_spouses_aboard int          not null,
  survived                   bit          not null
);


