package com.api.domain.model

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "people")
data class Person (

    @Id
    @Column(length = 64)
    var uuid: String = UUID.randomUUID().toString(),

    @Column(nullable = false)
    val survived: Boolean = false,

    @Column(nullable = false)
    val passengerClass: Int,

    @Column(nullable = false, length = 64)
    val name: String,

    @Column(nullable = false, length = 6)
    val sex: CharArray,

    @Column(nullable = false)
    val age: Int,

    @Column(nullable = false)
    val fare: Double,

    @Column(nullable = false)
    val parentsOrChildrenAboard: Int = 0,

    @Column(nullable = false)
    val siblingsOrSpousesAboard: Int = 0
)