package com.api.presentation.controller

import org.springframework.boot.web.servlet.error.ErrorController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController

@RestController
class NotFoundErrorController: ErrorController {

    override fun getErrorPath(): String {

        return "/error"
    }

    @ResponseBody
    @RequestMapping("/error")
    fun error(): Map<String, String?> {

        return mapOf("content" to "Resource not found")
    }

}